# README #

This is the repository for slides and Python code from the UCSD class on a very basic introduction to Deep Learning. Please download the iPython Notebooks before the class starts. 

## Getting to the presentation and source code ##

To access any of the presentation and code, on the left-hand corner, click on the "src" button: 
![Screen Shot 2015-08-20 at 7.20.32 AM.png](https://bitbucket.org/repo/aXKqdX/images/3468713259-Screen%20Shot%202015-08-20%20at%207.20.32%20AM.png)

You should see three files (the Preclass preparation, created by Natan Jacobson), the original presentation that I'll be giving, and this readme. You should also see a directory called "python-code". 

## Downloading the source code ##

For the class, you'll need to go into that directory, click on "all-notebooks.zip" and get the *.zip file. To do that, click on "raw", as shown here:
![Screen Shot 2015-08-20 at 7.30.39 AM.png](https://bitbucket.org/repo/aXKqdX/images/3744669102-Screen%20Shot%202015-08-20%20at%207.30.39%20AM.png)

## Downloading everything ##

If you want to download everything (a bit unnecessary), then simply click on the "downloads" button in the left-hand corner. (It's the thing that looks like a cloud with an arrow pointing down.) Then, you'll have access to the entire zip file:
![Screen Shot 2015-08-20 at 7.32.11 AM.png](https://bitbucket.org/repo/aXKqdX/images/185546612-Screen%20Shot%202015-08-20%20at%207.32.11%20AM.png)